package com.example.catapplication.ui.model

data class CatsUiModel (
    val id: String,
    val name: String,
    val temperament: String,
    val countryCode: String,
    val description: String,
    val wikipediaUrl: String,
    val imageUrl: String,
)