package com.example.catapplication.ui

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.catapplication.ui.screens.CatsViewModel
import com.example.catapplication.ui.screens.LoginScreen
import com.example.catapplication.ui.screens.LoginViewModel


@Composable
fun CatsApp(modifier: Modifier = Modifier) {
    Scaffold (
        modifier = modifier.fillMaxSize()
    )
    {
        Surface (
            modifier = Modifier
                .fillMaxSize()
                .padding(it),
            color = MaterialTheme.colors.background
        ) {
            val loginViewModel: LoginViewModel = viewModel()
            LoginScreen(
                loginViewModel = loginViewModel
            )
        }
    }
}