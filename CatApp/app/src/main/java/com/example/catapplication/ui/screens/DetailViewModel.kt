package com.example.catapplication.ui.screens

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.catapplication.data.apiservice.CatsApi
import com.example.catapplication.data.apiservice.model.CatsDto
import com.example.catapplication.ui.model.CatsUiModel
import com.example.catapplication.ui.model.mapper.CatsDtoUiMapper
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class DetailViewModel : ViewModel() {

    private val _cat = MutableStateFlow<CatsUiModel>(CatsUiModel("","","","","","",""))
    val cat: StateFlow<CatsUiModel> = _cat.asStateFlow()

    var mapper = CatsDtoUiMapper()

    init {
        //loadCat()
    }

    fun loadCat(id: String) {


        viewModelScope.launch {
            val catResult = CatsApi.retrofitService.getBreed(id)

            val catImage = CatsApi.retrofitService.getCatImageRx(id)
            for (image in catImage){
                catResult.imageUrl = image.imageUrl
            }

            _cat.value = mapper.map(catResult)
        }

    }




}