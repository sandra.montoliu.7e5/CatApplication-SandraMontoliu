package com.example.catapplication.ui.screens

import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.ClickableText
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.*
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.bumptech.glide.integration.compose.ExperimentalGlideComposeApi
import com.bumptech.glide.integration.compose.GlideImage
import com.example.catapplication.ui.model.CatsUiModel
import com.example.catapplication.ui.theme.CatsPhotosTheme


class DetailScreen : ComponentActivity() {
    override fun onCreate (savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            val context = LocalContext.current
            val intent = (context as DetailScreen).intent

            val detailViewModel = DetailViewModel()

            CatsPhotosTheme {
                val id = intent.getStringExtra("catId")
                detailViewModel.loadCat(id.toString())
                val cat by detailViewModel.cat.collectAsState()
                Detail(cat = cat)
            }
        }
    }
}


@OptIn(ExperimentalGlideComposeApi::class)
@Composable
private fun Detail(
    cat: CatsUiModel,
    modifier: Modifier = Modifier
){
    Log.w("Detail","Start Detail")
    Log.w("Detail",cat.id)
    val annotatedText = buildAnnotatedString {
        append("Wikipedia")
        pushStringAnnotation("url",cat.wikipediaUrl)
    }


    Column(modifier = modifier) {
        GlideImage(
            model = cat.imageUrl,
            contentDescription = "catImage",
            modifier = modifier
                .height(300.dp)
                .padding(15.dp, 15.dp, 15.dp, 0.dp)
                .align(Alignment.CenterHorizontally)
        )
        Row(modifier = modifier
            .padding(0.dp, 10.dp)
            .align(Alignment.CenterHorizontally)
        ) {
            Text(
                text = cat.name,
                modifier = modifier
                    .weight(0.3f)
                    .padding(15.dp, 0.dp, 0.dp, 0.dp),
                textAlign = TextAlign.Start,
                fontSize = 20.sp
            )
            Spacer(modifier = modifier
                //.padding(0.dp,0.dp,0.dp,0.dp)
                .weight(0.4f))
            Text(
                text = cat.countryCode,
                modifier = modifier
                    .weight(0.2f)
                    .padding(0.dp, 0.dp, 30.dp, 0.dp),
                textAlign = TextAlign.End,
                fontSize = 15.sp,
            )
        }
        Text(
            text = cat.temperament,
            modifier = modifier
                .padding(15.dp,10.dp,15.dp,0.dp),
            fontSize = 16.sp,
        )
        Text(
            text = cat.description,
            modifier = modifier
                .padding(15.dp,10.dp,15.dp,0.dp),
            fontSize = 16.sp,
        )
        ClickableText(
            text = annotatedText,
            onClick = { offset ->

            })
    }
}