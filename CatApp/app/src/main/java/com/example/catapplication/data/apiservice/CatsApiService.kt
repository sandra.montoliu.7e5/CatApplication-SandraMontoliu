package com.example.catapplication.data.apiservice


import com.example.catapplication.data.apiservice.model.CatImageDto
import com.example.catapplication.data.apiservice.model.CatsDto
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonConfiguration

import okhttp3.MediaType
import retrofit2.Retrofit
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

private const val BASE_URL = "https://api.thecatapi.com/v1/"


private val retrofit = Retrofit.Builder()
    .addConverterFactory(
        Json {
            ignoreUnknownKeys = true
        }
        .asConverterFactory(MediaType.get("application/json"))
        )
    .baseUrl(BASE_URL)
    .build()


object CatsApi {
    val retrofitService: CatsApiService by lazy {
        retrofit.create(CatsApiService::class.java)
    } // lazy -> Fins que no s'utilitza no es crea "MarsApiService"
}

interface CatsApiService {
    // Aquí aniran les funcions que criden a la API

    @GET("breeds") // Llista de races
    suspend fun getCatsRx(): List<CatsDto>

    @GET("images/search") // Imatge a partir d'un id
    suspend fun getCatImageRx(@Query("breed_id") id: String): List<CatImageDto>

    @GET("breeds/{id}")  // Raça a partir d'un id
    //suspend fun getBreed(@Path("breed_id") id: String): CatsDto
    suspend fun getBreed(@Path("id") id: String): CatsDto
}