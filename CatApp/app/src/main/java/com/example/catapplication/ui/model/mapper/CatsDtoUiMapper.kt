package com.example.catapplication.ui.model.mapper

import com.example.catapplication.data.apiservice.model.CatsDto
import com.example.catapplication.ui.model.CatsUiModel

class CatsDtoUiMapper {
    fun map(listDto: List<CatsDto>): List<CatsUiModel> {
        return mutableListOf<CatsUiModel>().apply {
            listDto.forEach {
                this.add(
                    CatsUiModel(
                        id = it.id,
                        name = it.name,
                        temperament = it.temperament,
                        countryCode = it.countryCode,
                        description = it.description,
                        wikipediaUrl = it.wikipedia_url,
                        imageUrl = it.imageUrl
                    )
                )
            }
        }
    }

    fun map(dto: CatsDto) : CatsUiModel {
        return  CatsUiModel (
            id = dto.id,
            name = dto.name,
            temperament = dto.temperament,
            countryCode = dto.countryCode,
            description = dto.description,
            wikipediaUrl = dto.wikipedia_url,
            imageUrl = dto.imageUrl
        )
    }
}