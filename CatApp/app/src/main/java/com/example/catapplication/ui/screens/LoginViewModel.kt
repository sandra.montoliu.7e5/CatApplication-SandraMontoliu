package com.example.catapplication.ui.screens

import android.content.Context
import android.content.Intent
import android.os.Debug
import android.util.Log
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.ui.platform.LocalContext
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow

class LoginViewModel : ViewModel() {

    // Login UI state
    //private val _uiState = MutableStateFlow(LoginUiState())
    // Valor read-only
    //val uiState: StateFlow<LoginUiState> = _uiState.asStateFlow()


    var userName by mutableStateOf("")
        private set
    var userPassword by mutableStateOf("")
        private set

    fun updateUserName(guessedName: String) {
        userName = guessedName
    }

    fun updateUserPassword(guessedPasword: String) {
        userPassword = guessedPasword
    }


    fun checkUser(mContext: Context) {

        // Es comprovaria que el nom d'usuari i la contrasenya siguin correctes.
        // * Se comprovaria que el nombre de usuario y la contraseña sean correctos.


        // Pasar a la seguent pantalla ---------------------------------------------------
        val intent = Intent(mContext, HomeScreen::class.java)
        //Log.d("checking user", "checking user") // Funciona
        mContext.startActivity(intent)
    }
}
