package com.example.catapplication.ui.screens

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Button
import androidx.compose.material.OutlinedButton
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp


@Composable
fun LoginScreen(
    modifier: Modifier = Modifier,
    loginViewModel: LoginViewModel
) {
    val mContext = LocalContext.current

    Column (
        modifier = modifier
    ) {
        UserGuess(
            onNameGuessChanged = { loginViewModel.updateUserName(it) },
            onPasswordGuessChanged = { loginViewModel.updateUserPassword(it) },
            nameGuess = loginViewModel.userName,
            passwordGuess = loginViewModel.userPassword,
            onKeyboardDone = { loginViewModel.checkUser(mContext) }
        )
        Button(
            modifier = modifier
                .fillMaxWidth()
                .padding(30.dp,30.dp,30.dp, 0.dp),
            onClick = { loginViewModel.checkUser(mContext) }
        ) {
            Text("Submit", fontSize = 20.sp)
        }
    }
}

@Composable
fun UserGuess(
    modifier: Modifier = Modifier,
    nameGuess: String,
    passwordGuess: String,
    onNameGuessChanged: (String) -> Unit,
    onPasswordGuessChanged: (String) -> Unit,
    onKeyboardDone: () -> Unit
    ) {
    Column(
        modifier = modifier.padding(20.dp, 5.dp, 20.dp, 0.dp)
    ) {
        OutlinedTextField(
            value = nameGuess,
            singleLine = true,
            modifier = Modifier.fillMaxWidth(),
            onValueChange = onNameGuessChanged,
            label = { Text(text = "Username") }
        )

        Spacer(modifier = modifier.padding(0.dp, 15.dp))

        OutlinedTextField(
            value = passwordGuess,
            singleLine = true,
            modifier = Modifier.fillMaxWidth(),
            onValueChange = onPasswordGuessChanged,
            label = { Text(text = "Password") }
        )
    }

}