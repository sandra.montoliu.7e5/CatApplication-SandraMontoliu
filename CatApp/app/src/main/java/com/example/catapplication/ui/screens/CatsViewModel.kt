package com.example.catapplication.ui.screens

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
//import com.example.catapplication.data.apiservice.CatsApi
import com.example.catapplication.ui.model.CatsUiModel
import com.example.catapplication.ui.model.mapper.CatsDtoUiMapper
import kotlinx.coroutines.launch

class CatsViewModel : ViewModel() {
    // Obtindre la informació de la api per a mostrarla.
    // Utilitza CatsDtoUiMapper

    var catsUiState: List<CatsUiModel> by mutableStateOf(emptyList())
        private set

    var mapper = CatsDtoUiMapper()

    // Para poder mostrar informacion justo al iniciar
    init {
        getCats()
    }

    fun getCats() {
        viewModelScope.launch {
            //val listResult = CatsApi.retrofitService.getPhotos()
            //catsUiState = mapper.map(listResult)
        }
    }
}