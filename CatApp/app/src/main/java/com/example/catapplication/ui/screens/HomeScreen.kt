
package com.example.catapplication.ui.screens

import android.content.Intent
import android.os.Bundle
import android.os.Debug
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.bumptech.glide.integration.compose.ExperimentalGlideComposeApi
import com.bumptech.glide.integration.compose.GlideImage
import com.example.catapplication.ui.model.CatsUiModel
import com.example.catapplication.ui.theme.CatsPhotosTheme


class HomeScreen : ComponentActivity() {
    override fun onCreate (savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            Log.w("1","Start HomeScreen Activity")
            CatHome()
        }
    }
}

@Preview
@Composable
fun CatCardPreview(){
    CatCard(cat = CatsUiModel("1","CatName","Temperament","Code","Description","WikiUrl","https://cdn2.thecatapi.com/images/KWdLHmOqc.jpg"))
}


@OptIn(ExperimentalMaterialApi::class, ExperimentalGlideComposeApi::class)
@Composable
fun CatCard(cat: CatsUiModel, modifier: Modifier = Modifier) {
    Log.w("4","Start CatCard")
    val mContext = LocalContext.current
    val intent = Intent(mContext, DetailScreen::class.java)

    // Card UI
    Card (
        modifier = modifier
            .fillMaxWidth()
            .padding(0.dp,3.dp),
        elevation = 10.dp,
        shape = RoundedCornerShape(0.dp),
        onClick = {
            intent.putExtra("catId", cat.id)
            mContext.startActivity(intent)
        }
        ) {
        Row() {
            GlideImage(
                model = cat.imageUrl,
                contentDescription = "catImage",
                modifier = modifier
                    .size(150.dp)
                    .padding(0.dp,0.dp,0.dp,0.dp)
                    .align(Alignment.CenterVertically)
            )
            Column(
                modifier = modifier
                    .padding(15.dp,10.dp,15.dp,10.dp)
                ) {
                Text(
                    text = cat.name,
                    modifier = modifier,
                    fontSize = 20.sp
                )

                Text(
                    text = cat.description,
                    modifier = modifier,
                    fontSize = 15.sp,
                    maxLines = 5,
                    overflow = TextOverflow.Ellipsis
                )
            }
        }
    }
}

@Composable
fun CatsList(catsList: List<CatsUiModel>, modifier: Modifier = Modifier) {
    Log.w("3","Start CatList")
    //if (catsList.isEmpty()) Log.w("3.5","Empty list")

    // Wrap cards in a lazy colum
    LazyColumn(modifier = Modifier.background(MaterialTheme.colors.background)){
        items(catsList) { cat ->
            CatCard(cat = cat)
        }
    }
    //CatCardPreview()
}

@Composable
fun CatHome(homeViewModel: HomeViewModel = viewModel()) {
    Log.w("2","Start CatHome")
    // Applay Theme and affirmation list
    //val homeViewModel = HomeViewModel()
    //homeViewModel.loadList()
    val cats by homeViewModel.catsList.collectAsState()

    CatsPhotosTheme() {
        CatsList(catsList = cats)
    }
}