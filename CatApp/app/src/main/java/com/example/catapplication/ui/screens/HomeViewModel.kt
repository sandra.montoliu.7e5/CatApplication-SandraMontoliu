package com.example.catapplication.ui.screens

import android.util.Log
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.catapplication.data.apiservice.CatsApi
import com.example.catapplication.ui.model.CatsUiModel
import com.example.catapplication.ui.model.mapper.CatsDtoUiMapper
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import kotlinx.serialization.json.Json


class HomeViewModel : ViewModel() {

    private val _catsList = MutableStateFlow<List<CatsUiModel>>(mutableListOf())
    val catsList: StateFlow<List<CatsUiModel>> = _catsList.asStateFlow()

    var mapper = CatsDtoUiMapper()

    init {
        loadList()
    }

    fun loadList(){

        viewModelScope.launch {
            val listResult = CatsApi.retrofitService.getCatsRx()

            for (result in listResult){

                val catImage = CatsApi.retrofitService.getCatImageRx(result.id)

                for (image in catImage){
                    result.imageUrl = image.imageUrl
                }

            }
            _catsList.value = mapper.map(listResult)
        }
    }




}